﻿
namespace JsonApp
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Welcome
    {
        [JsonProperty("definitions")]
        public Definition[] Definitions { get; set; } // this it the Definitions array

        [JsonProperty("word")]
        public string Word { get; set; }

        [JsonProperty("pronunciation")]
        public string Pronunciation { get; set; }
    }

    

    public partial class Definition
    {
        [JsonProperty("type")]
        public string Type { get; set; } // gets the type of the word 

        [JsonProperty("definition")]
        public string DefinitionDefinition { get; set; } //gets the definition of the word

        [JsonProperty("example")]
        public string Example { get; set; }// gets the example of the word 

        [JsonProperty("image_url")]
        public Uri ImageUrl { get; set; }

        [JsonProperty("emoji")]
        public string Emoji { get; set; }
    }

    public partial class Definition
    {
        public static Definition[] FromJson(string json) => JsonConvert.DeserializeObject<Definition[]>(json, JsonApp.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Definition[] self) => JsonConvert.SerializeObject(self, JsonApp.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}

