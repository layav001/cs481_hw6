﻿using Xamarin.Essentials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Net.Http;

namespace JsonApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            //this makes a pop up when you load into the app with no internet. 
            if (Connectivity.NetworkAccess != NetworkAccess.Internet)
                DisplayAlert("Error: You are not connected to the internet.", "", "OK");
            InitializeComponent();

        }

        async void Button1_Clicked(object sender, EventArgs e)
        {
            //Checks to see if there is an internet connection, if not then then it displays a pop up letting the user know
            if (Connectivity.NetworkAccess != NetworkAccess.Internet)
            {
                error.Text = "You are not connected to the internet.";
                current.Text = "";
                Def.Text = "";
                Typ.Text = "";
                Ex.Text = "";
                //await DisplayAlert("Error: You are not connected to the internet.", "", "OK");
            }
            else
            { 

            //this allows us to pull data from the owlbot api 
            var Search = Enter.Text; 
         
            HttpClient client = new HttpClient(); // creates the http client 
            var uri = new Uri(string.Format("https://owlbot.info/api/v2/dictionary/") + Search); // api 

            //what gets built to send to the server 
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;

            //what the server builds to send back to us 
            HttpResponseMessage response = await client.SendAsync(request);
            Definition[] word = null;


            //if statement that checks to see if the usr input is empty when searching or when there is a space
            if( (String.IsNullOrEmpty(Search) || Search == " " ) == true)
                {
                    error.Text = "Error: no word entered";
                    current.Text = ""; //display the word
                    Def.Text = "";//display the definition
                    Typ.Text = "";//display the type
                    Ex.Text = "";//display an example.
                    return; 
                }
            //if the response from the server is susccessful in retrieveing data from the json file then it displays the users word and infomration about it 
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                word = Definition.FromJson(content);

                error.Text = "";
                current.Text = "Word: " + Search; //display the word
                Def.Text = "Definition: " + word[0].DefinitionDefinition;//display the definition
                Typ.Text = "Type: " + word[0].Type;//display the type
                Ex.Text = "Example: " + word[0].Example;//display an example.
            }
            
            //provides error message when it is unable to find the users word and or invalid word 
            else if(!response.IsSuccessStatusCode)
            {

                error.Text = "Error: Please enter a valid word";
                current.Text = ""; //display the word
                Def.Text = "";//display the definition
                Typ.Text = "";//display the type
                Ex.Text = "";//display an example.

            }
            }
        }

        //this displays a pop up message when there is no internet connection 
        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            if (Connectivity.NetworkAccess != NetworkAccess.Internet)
                 await DisplayAlert("Error: You are not connected to the internet.", "", "OK");
        }


    }
}
